export const getters = {
};

export const strict = false;


export const state = () => ({
  blue :  null,
  jaune : null,
  green : null

});

export const mutations = {
  storeblue : (state,value) => state.blue = value,
  storejaune : (state,value) => state.jaune = value,
  storegreen : (state,value) => state.green = value,
};
